import sqlite3
import psycopg2
import time

from bot.models import Horoscope, Account
from telegram.error import BadRequest
from django_horoskope_tg_bot.settings import DATABASES


ZODIAC = {
    "Телець": u"\u2649",
    "Рак": u"\u264B",
    "Стрілець": u"\u2650",
    "Козеріг": u"\u2651",
    "Скорпіон": u"\u264F",
    "Водолій": u"\u2652",
    "Риби": u"\u2653",
    "Діва": u"\u264D",
    "Овен": u"\u2648",
    "Лев": u"\u264C",
    "Терези": u"\u264E",
    "Близнюки": u"\u264A"
}


def database_connection():
    db_connection = None

    database = DATABASES.get('default', {})
    if database.get('ENGINE', '').split('.')[-1] == 'postgresql':
        db_connection = psycopg2.connect(user=database.get('USER'),
                                         password=database.get('PASSWORD'),
                                         host=database.get('HOST'),
                                         database=database.get('NAME'))

    elif database.get('ENGINE', '').split('.')[-1] == 'sqlite3':
        db_connection = sqlite3.connect(database.get('NAME'))

    return db_connection


def get_horoscope_text(sign):
    global ZODIAC

    try:
        horoscope = Horoscope.objects.get(sign=sign)
        return f"{ZODIAC.get(sign, '')} {horoscope.sign} - {horoscope.day} {horoscope.month}\n\n{horoscope.text}"
    except (Exception,):
        return None


def is_subscribed(bot, chat_id, user_id):
    try:
        chat_member = bot.getChatMember(chat_id=chat_id, user_id=user_id)
        if chat_member.status in ['administrator', 'creator', 'member']:
            return True
        else:
            return False
    except BadRequest:
        return False


def send_notify(bot, chat_id, reply_markup, seconds=60):
    time.sleep(seconds)
    try:
        bot.sendMessage(chat_id=chat_id,
                        text="Бажаєте подивитись інформацію по іншому знаку?",
                        reply_markup=reply_markup)
    except (Exception,):
        pass
    else:
        acc = Account.objects.get(chat_id=chat_id)
        acc.push_sent = True
        acc.save(update_fields=['push_sent'])
