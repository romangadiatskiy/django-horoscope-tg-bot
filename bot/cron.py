from src.parser import HoroscopeParser
from src.sender import NotifyPushMessage


def cron_parser():
    HoroscopeParser().run()


def cron_sender():
    NotifyPushMessage().run()
