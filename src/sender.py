import os
import sys
import django
import telegram
import logging
import datetime
import pytz

from django_horoskope_tg_bot.settings import BASE_DIR

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "django_horoskope_tg_bot.settings")
sys.path.append(BASE_DIR)

django.setup()

from bot.models import Notify
from bot.views import bot
from src.handler import ZODIAC


class NotifyPushMessage:
    def __init__(self, tg_bot: telegram.Bot = bot) -> None:
        self._bot = tg_bot
        self._notifies = Notify.objects.filter(is_sent=False).distinct('user__username')
        self._columns = 2
        self._buttons = [
            telegram.InlineKeyboardButton(f"Відписатися від розсилки".strip(), callback_data="unsubscribed"),
            telegram.InlineKeyboardButton(f"Змінити знак Зодіаку".strip(), callback_data="changeSign")
        ]

    @staticmethod
    def time_in_range(current,
                      start=datetime.time(8, 0, 0),
                      end=datetime.time(22, 0, 0)):

        """Return true if x is in the range [start, end]"""
        if start <= end:
            return start <= current <= end
        else:
            return start <= current or current <= end

    def send_notify(self, notify: Notify) -> None:
        try:
            text = f"{ZODIAC.get(notify.message.sign, '')} " \
                   f"{notify.message.sign} - {notify.message.day} {notify.message.month}" \
                   f"\n\n{notify.message.text}"

            reply_markup = telegram.InlineKeyboardMarkup(
                [self._buttons[i:i + self._columns] for i in range(0, len(self._buttons), self._columns)])

            self._bot.sendMessage(chat_id=notify.user.chat_id, text=text, reply_markup=reply_markup)
        except (Exception,) as e:
            logging.warning(f"Sending notify error [{notify.user.username}]:: {e}")
        else:
            try:
                notify.is_sent = True
                notify.save(update_fields=['is_sent'])
            except (Exception,) as e:
                logging.warning(f"Can't update notify status for {notify.user.username}:: {e}")
        finally:
            logging.info(f"{notify}")

    def run(self):
        tz = pytz.timezone('Europe/Kiev')
        if self.time_in_range(current=datetime.datetime.now(tz=tz).time()):
            for push in self._notifies:
                self.send_notify(notify=push)


if __name__ == '__main__':
    NotifyPushMessage().run()
