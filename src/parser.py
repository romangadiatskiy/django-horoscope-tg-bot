import os
import sys
import re
import requests
import django
import logging

from bs4 import BeautifulSoup

from django_horoskope_tg_bot.settings import BASE_DIR

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "django_horoskope_tg_bot.settings")
sys.path.append(BASE_DIR)

django.setup()

from bot.models import Horoscope, Account, Notify


REQUEST_ATTEMPTS = 1


class HoroscopeParser:
    def __init__(self):
        self.base_url = 'https://1plus1.ua/'
        self.session = requests.Session()
        self.session.headers.update({
            "Host": "1plus1.ua",
            "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:93.0) Gecko/20100101 Firefox/93.0",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
            "Accept-Language": "en-US,en;q=0.5",
            "Accept-Encoding": "gzip, deflate, br",
            "DNT": "1",
            "Connection": "keep-alive",
            "Upgrade-Insecure-Requests": "1",
            "Sec-Fetch-Dest": "document",
            "Sec-Fetch-Mode": "navigate",
            "Sec-Fetch-Site": "none",
            "Sec-Fetch-User": "?1",
            "Sec-GPC": "1"
        })

        self.normalize = {
            'Овнів': 'Овен',
            'Тільців': 'Телець',
            'Тельців': 'Телець',
            'Близнюків': 'Близнюки',
            'Раків': 'Рак',
            'Левів': 'Лев',
            'Дів': 'Діва',
            'Терезів': 'Терези',
            'Скорпіонів': 'Скорпіон',
            'Стрільців': 'Стрілець',
            'Козерогів': 'Козеріг',
            'Водоліїв': 'Водолій',
            'Риб': 'Риби'
        }

    def make_request(self, url, method='get', **kwargs):
        for _ in range(REQUEST_ATTEMPTS):
            try:
                response = self.session.request(method=method, url=url, **kwargs)
                if response and not response.ok:
                    raise Exception(response.text)
            except (Exception,) as e:
                logging.warning(f"{method} Can't make request {url}:: {e}")
            else:
                return response
        return None

    def get_horoscope_url(self):
        response = self.make_request(url=self.base_url)
        if response:
            soup = BeautifulSoup(response.text, 'html.parser')
            a = soup.find('a', attrs={'href': re.compile(pattern=f'/.*?/novyny/goroskop')})

            try:
                if a and a['href'].startswith(self.base_url):
                    return a['href']
            except (Exception,):
                pass

            return os.path.join(self.base_url, a['href'].lstrip('/ru')) if a else None
        else:
            return None

    def parse_horoscope(self, url):
        response = self.make_request(url=url)
        if response:
            soup = BeautifulSoup(response.text, 'html.parser')
            div = soup.find('div', attrs={'class': 'article__content'})
            items = div.find_all('p') if div else []
            for item in items:
                if item.strong and item.strong.text.startswith('Гороскоп'):
                    search = re.search(pattern=f"Гороскоп на (\d+)\s(.*)\sдля\s(\w+)",
                                       string=item.text)

                    try:
                        day = search[1]
                    except (Exception,) as e:
                        logging.warning(f"Can't parse a day:: {e}")
                        day = None

                    try:
                        month = search[2]
                    except (Exception,) as e:
                        logging.warning(f"Can't parse a month:: {e}")
                        month = None

                    try:
                        sign = self.normalize.get(search[3], '')
                    except (Exception,) as e:
                        logging.warning(f"Can't parse a sign:: {e}")
                        sign = None

                    try:
                        text = item.text.replace(item.strong.text, '').split('\n')[-1].replace(' ', '').strip()
                    except (Exception,) as e:
                        logging.warning(f"Can't parse a text:: {e}")
                        text = None

                    logging.info(f"{sign} -> {day} {month}: {text}")

                    if day and month and sign and text:
                        horoscope, _ = Horoscope.objects.get_or_create(sign=sign)

                        if int(horoscope.day) != int(day) or horoscope.month != month:
                            try:
                                accounts = Account.objects.filter(is_subscribed=True, sign=sign)
                            except (Exception,) as e:
                                logging.warning(
                                    f"Can't get a list of account with sign {sign} for creating a notify:: {e}")
                            else:
                                for account in accounts:
                                    try:
                                        Notify.objects.create(user=account, message=horoscope)
                                    except (Exception,) as e:
                                        logging.warning(
                                            f"Can't create a notify in database for {account.username}:: {e}")

                        horoscope.text = text
                        horoscope.day = day
                        horoscope.month = month
                        horoscope.save()

    def run(self):
        url = self.get_horoscope_url()
        logging.info(f'Horoscope URL: {url}')

        if url:
            self.parse_horoscope(url=url)
        else:
            logging.info("Horoscope url has not found")


if __name__ == '__main__':
    HoroscopeParser().run()
