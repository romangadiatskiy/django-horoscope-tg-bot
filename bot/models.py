from django.db import models


class Account(models.Model):
    id = models.AutoField(primary_key=True)
    chat_id = models.BigIntegerField('Chat ID', unique=True)

    username = models.CharField('Username', max_length=200, unique=False, null=True, blank=True)
    first_name = models.CharField('First Name', max_length=200, null=True, blank=True)
    last_name = models.CharField('Last Name', max_length=200, null=True, blank=True)
    description = models.TextField('Description', null=True, blank=True)
    bio = models.TextField('Bio', null=True, blank=True)
    sign = models.TextField('Sign', null=True, blank=True)

    is_subscribed = models.BooleanField('Is Subscribed', default=True)
    push_sent = models.BooleanField('Push sent', default=False)

    modified = models.DateTimeField("Modified", auto_now=True)
    created = models.DateTimeField('Created', auto_now_add=True, blank=True, null=True)

    class Meta:
        verbose_name = 'Account'
        verbose_name_plural = 'Accounts'
        ordering = ('username', 'created')

    def __str__(self):
        return self.username


class Message(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(Account, on_delete=models.CASCADE)
    text = models.TextField(default='')

    modified = models.DateTimeField("Modified", auto_now=True)
    created = models.DateTimeField('Created', auto_now_add=True, blank=True, null=True)

    class Meta:
        verbose_name = 'Message'
        verbose_name_plural = 'Messages'
        ordering = ('user__username', 'created')


class Horoscope(models.Model):
    id = models.AutoField(primary_key=True)
    sign = models.CharField('Sign', max_length=20)
    day = models.IntegerField('Day', null=True, blank=True)
    month = models.CharField('Day', max_length=100, default='')
    text = models.TextField('Text', default='')

    modified = models.DateTimeField("Modified", auto_now=True)
    created = models.DateTimeField('Created', auto_now_add=True, blank=True, null=True)

    class Meta:
        verbose_name = 'Horoscope'
        verbose_name_plural = 'Horoscopes'
        ordering = ('sign', 'created')


class Notify(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(Account, on_delete=models.CASCADE)
    message = models.ForeignKey(Horoscope, on_delete=models.CASCADE)

    is_sent = models.BooleanField('Is Sent', default=False)

    modified = models.DateTimeField("Modified", auto_now=True)
    created = models.DateTimeField('Created', auto_now_add=True, blank=True, null=True)
