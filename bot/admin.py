from django.contrib import admin

from bot.models import *


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = ['username', 'first_name', 'last_name', 'description', 'bio', 'modified', 'created']

    list_filter = ['username', 'first_name', 'last_name']
    search_fields = ('username', 'first_name', 'last_name')
    exclude = ()


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ['user', 'text', 'modified', 'created']

    list_filter = ['user__username', 'text']
    search_fields = ('user__user_name', 'user__first_name', 'user__last_name')
    exclude = ()


@admin.register(Horoscope)
class HoroscopeAdmin(admin.ModelAdmin):
    list_display = ['sign', 'text', 'month', 'day', 'modified', 'created']

    list_filter = ['sign']
    search_fields = ('sign',)
    exclude = ()
