import os
import time
import re
import configparser
import telegram
import json
import logging
import threading

import pandas as pd

from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django_horoskope_tg_bot.settings import BASE_DIR

from bot.models import *
from src.handler import get_horoscope_text, is_subscribed, database_connection, send_notify, ZODIAC
from src.queries import USERS_LIST_QUERY

from pprint import pprint

config_path = os.path.join(BASE_DIR, 'config.ini')
config = configparser.RawConfigParser()
config.read(config_path)

TOKEN = config.get('TELEGRAM', 'API_TOKEN')
URL = config.get('TELEGRAM', 'HOOK_URL')

logging.info(f'Token: {TOKEN} Url: {URL}')
bot = telegram.Bot(token=TOKEN)
signs = Horoscope.objects.all().values_list('sign', flat=True).distinct()


def index(request):
    return JsonResponse({'Horoscope TG bot': 'Started', 'status_code': 200})


def set_webhook(request):
    global URL
    global TOKEN

    logging.info(f'{URL}{TOKEN}')

    try:
        certificate_path = os.path.join(BASE_DIR, 'goroskopi.pem')
        s = bot.setWebhook(f'{URL}{TOKEN}', certificate=open(certificate_path, 'rb'))
    except (Exception,) as e:
        logging.warning(f"Can't set the webhook:: {e}")
        s = None

    if s:
        logging.info('webhook:', s)
        return HttpResponse("Webhook Setup: Ok".encode())
    else:
        return HttpResponse("Webhook Setup: Failed".encode())


@csrf_exempt
def respond(request, token):
    global signs

    if request.method == 'POST':
        subscribe_link = 'https://t.me/yourexua'
        tg_username = '@yourexua'

        # retrieve the message in JSON and then transform it to Telegram object
        data = json.loads(request.body)
        update = telegram.Update.de_json(data, bot)

        logging.info(update.to_dict())

        try:
            chat_id = update.message.chat.id
            username = update.message.chat.username
            first_name = update.message.chat.first_name
            last_name = update.message.chat.last_name
            description = update.message.chat.description
            bio = update.message.chat.description

            # Telegram understands UTF-8, so encode text for unicode compatibility
            text = update.message.text.encode('utf-8').decode()
        except:
            # Callback data from button
            try:
                chat_id = update.callback_query.message.chat.id
                username = update.callback_query.message.chat.username
                first_name = update.callback_query.message.chat.first_name
                last_name = update.callback_query.message.chat.last_name
                description = update.callback_query.message.chat.description
                bio = update.callback_query.message.chat.description

                text = update.callback_query.data.encode('utf-8').decode()
            except:
                try:
                    chat_id = update.my_chat_member.chat.id
                    status = update.my_chat_member.new_chat_member.status

                    if status == 'kicked':
                        Account.objects.filter(chat_id=chat_id).update(is_subscribed=False)
                    else:
                        raise Exception()

                    return JsonResponse({'ok': True, 'status_code': 200}, safe=False)
                except:
                    logging.warning(f"Callback data ERROR:: {update.to_dict()}")
                    return JsonResponse({'ok': True, 'status_code': 200}, safe=False)

        try:
            account, acc_is_created = Account.objects.get_or_create(chat_id=chat_id)
        except (Exception,) as e:
            account = None
            logging.warning(f"Can't get or create an account {chat_id}: {username}:: {e}")
        else:
            account.username = username
            account.first_name = first_name
            account.last_name = last_name
            account.description = description
            account.bio = bio

            account.save()

            try:
                Message.objects.create(user=account, text=text)
            except (Exception,) as e:
                logging.warning(f"Can't create a message {account.username}: {text}:: {e}")

        # for debugging purposes only
        logging.debug(f"got text message: {text}")

        columns = 2
        buttons = [telegram.InlineKeyboardButton(f"{ZODIAC.get(sign, '')} {sign}".strip(), callback_data=sign)
                   for sign in signs]

        signs_reply_markup = telegram.InlineKeyboardMarkup(
            [buttons[i:i + columns] for i in range(0, len(buttons), columns)])

        # the first time you chat with the bot AKA the welcoming message
        if text == "/start":

            # print the welcoming message
            bot_welcome = """
            Привіт, обери свій знак зодіака і отримай персоналізований/авторський класичний гороскоп на день.
            """

            # send the welcoming message
            try:
                bot.sendMessage(chat_id=chat_id,
                                text=bot_welcome,
                                reply_markup=signs_reply_markup)
            except (telegram.error.Unauthorized, Exception) as e:
                logging.warning(f"{chat_id} -> {username}:: {e}")

        elif text == 'subscribed':
            if is_subscribed(bot=bot, chat_id=tg_username, user_id=chat_id):
            # if account and account.is_subscribed:

                if account:
                    account.is_subscribed = True
                    account.save(update_fields=['is_subscribed'])

                try:
                    message = Message.objects.filter(user__username=username, text__in=signs).last()
                except (Exception,) as e:
                    logging.warning(f"Can't find user:: {username}:: {e}")
                else:
                    try:
                        bot.sendMessage(chat_id=chat_id,
                                        text="Чудово! Тепер ви можете користуватися всім функціоналом бота!")
                    except (telegram.error.Unauthorized, Exception) as e:
                        logging.warning(f"{chat_id} -> {username}:: {e}")
                        time.sleep(1)

                    horoscope_text = get_horoscope_text(sign=message.text)
                    if horoscope_text:
                        if account and not account.sign:
                            account.sign = message.text
                            account.save(update_fields=['sign'])

                        try:
                            bot.sendMessage(chat_id=chat_id, text=horoscope_text)
                        except (telegram.error.Unauthorized, Exception) as e:
                            logging.warning(f"{chat_id} -> {username}:: {e}")

                        threading.Thread(target=send_notify, args=[bot, chat_id, signs_reply_markup, 60]).start()
                    else:
                        try:
                            bot.sendMessage(chat_id=chat_id, text="Такий знак Зодіаку мені невідомий.")
                        except (telegram.error.Unauthorized, Exception) as e:
                            logging.warning(f"{chat_id} -> {username}:: {e}")

            else:
                if account:
                    account.is_subscribed = False
                    account.push_sent = False
                    account.save(update_fields=['is_subscribed', 'push_sent'])

                try:
                    bot.sendMessage(chat_id=chat_id, text="Схоже на те, що ви ще не підписались.")
                except (telegram.error.Unauthorized, Exception) as e:
                    logging.warning(f"{chat_id} -> {username}:: {e}")

        elif text == 'unsubscribed':
            try:
                Account.objects.filter(chat_id=chat_id).update(is_subscribed=False)
            except (Exception,) as e:
                logging.warning(f"Can't unsubscribe a user {username} - {chat_id}:: {e}")

            try:
                bot.sendMessage(chat_id=chat_id, text='Ви успішно відписалися від розсилки')
            except (telegram.error.Unauthorized, Exception) as e:
                logging.warning(f"{chat_id} -> {username}:: {e}")

        elif text == 'changeSign':
            columns = 2
            buttons = [telegram.InlineKeyboardButton(f"{ZODIAC.get(sign, '')} {sign}".strip(),
                                                     callback_data=f"swap to {sign}")
                       for sign in signs]

            change_sign_reply_markup = telegram.InlineKeyboardMarkup(
                [buttons[i:i + columns] for i in range(0, len(buttons), columns)])

            try:
                bot.sendMessage(chat_id=chat_id,
                                text="Оберіть новий знак Зодіаку для розсилки:",
                                reply_markup=change_sign_reply_markup)
            except (telegram.error.Unauthorized, Exception) as e:
                logging.warning(f"{chat_id} -> {username}:: {e}")

        elif text.startswith("swap to"):
            sign = text.lstrip("swap to").strip()
            if sign not in signs:
                message = "Такий знак Зодіаку мені невідомий."
            else:
                message = "Ви успішно змінили свій знак Зодіаку."

                try:
                    Account.objects.filter(chat_id=chat_id).update(sign=sign)
                except (Exception,) as e:
                    logging.warning(f"Can't change a sign [{sign}] for user {username}:: {e}")
                    message = "Ой, щось пішло не так :("

            try:
                bot.sendMessage(chat_id=chat_id, text=message)
            except (telegram.error.Unauthorized, Exception) as e:
                logging.warning(f"{chat_id} -> {username}:: {e}")

        else:
            horoscope_text = get_horoscope_text(sign=text)
            if horoscope_text:
                if not is_subscribed(bot=bot, chat_id=tg_username, user_id=chat_id):
                # if account and not account.is_subscribed:

                    if account:
                        account.is_subscribed = False
                        account.push_sent = False
                        if not account.sign:
                            account.sign = text

                        account.save(update_fields=['is_subscribed', 'push_sent', 'sign'])

                    subscribe_text_message = """
                    Ваш гороскоп готовий! Щоб продовжити потрібно підписатись на канал-спонсор прогнозу.
                    """
                    subscribe_button = [telegram.InlineKeyboardButton(text="Підписатись", url=subscribe_link)]
                    already_subscribed = [telegram.InlineKeyboardButton(text="Підписка оформлена",
                                                                        callback_data='subscribed')]

                    subscribe_reply_markup = telegram.InlineKeyboardMarkup([subscribe_button, already_subscribed])
                    try:
                        bot.sendMessage(
                            chat_id=chat_id,
                            text=subscribe_text_message,
                            reply_markup=subscribe_reply_markup
                        )
                    except (telegram.error.Unauthorized, Exception) as e:
                        logging.warning(f"{chat_id} -> {username}:: {e}")
                else:
                    if account:
                        account.is_subscribed = True
                        if not account.sign:
                            account.sign = text

                        account.save(update_fields=['is_subscribed', 'push_sent', 'sign'])

                    try:
                        bot.sendMessage(chat_id=chat_id, text=horoscope_text)
                    except (telegram.error.Unauthorized, Exception) as e:
                        logging.warning(f"{chat_id} -> {username}:: {e}")
            else:
                try:
                    bot.sendMessage(chat_id=chat_id, text="Такий знак Зодіаку мені невідомий.")
                except (telegram.error.Unauthorized, Exception) as e:
                    logging.warning(f"{chat_id} -> {username}:: {e}")

        return JsonResponse({'ok': True, 'status_code': 200}, safe=False)


def users_dashboard(request):
    cnx = database_connection()
    df = pd.read_sql(USERS_LIST_QUERY, cnx)
    cnx.close()

    html_table = df.to_html(index=False, table_id='usersDashboardTable', escape=False, render_links=True)
    html_table = (html_table
                  .replace('class="dataframe"', 'class="display"')
                  .replace('border="1"', ''))

    return render(request, 'bot/users.html', context={
        'html_table': html_table
    })
