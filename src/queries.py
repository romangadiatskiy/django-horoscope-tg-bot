USERS_LIST_QUERY = """
SELECT ba.username as Username,
       COALESCE(ba.first_name, '') as First_Name,
       COALESCE(ba.last_name, '') as Last_Name,
       COALESCE(ba.bio, '') as Biography,
       ba.sign as Sign,
       ba.is_subscribed as Subscribed,
       count(bm.text) as Actions
FROM bot_account ba
JOIN bot_message bm on ba.id = bm.user_id
WHERE bm.text in (select sign from bot_horoscope)
GROUP BY ba.username,
         ba.first_name,
         ba.last_name,
         ba.bio,
         ba.sign,
         ba.is_subscribed
order by Actions desc;
"""