# Generated by Django 3.2.8 on 2021-10-23 14:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('bot', '0003_auto_20211017_1324'),
    ]

    operations = [
        migrations.AlterField(
            model_name='account',
            name='is_subscribed',
            field=models.BooleanField(default=True, verbose_name='Is Subscribed'),
        ),
        migrations.CreateModel(
            name='Notify',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('is_sent', models.BooleanField(default=False, verbose_name='Is Sent')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='Modified')),
                ('created', models.DateTimeField(auto_now_add=True, null=True, verbose_name='Created')),
                ('message', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='bot.horoscope')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='bot.account')),
            ],
        ),
    ]
