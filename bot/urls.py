from django.urls import path

from bot import views


urlpatterns = [
    path("", views.index, name='index'),
    path("webhook/setWebhook/", views.set_webhook, name='set_webhook'),
    path("<token>", views.respond, name='respond'),
    path("users/dashboard/", views.users_dashboard, name='usersDashboard'),
]
